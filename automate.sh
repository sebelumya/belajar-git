#!/bin/sh
FOLDER="$1 at `date`";
mkdir "$FOLDER";

ABOUTME="$FOLDER/about_me";
mkdir "$ABOUTME";

mkdir "$ABOUTME/personal"
touch "$ABOUTME/personal/facebook.txt";
echo "https://www.facebook.com/$2" > "$ABOUTME/personal/facebook.txt";

mkdir "$ABOUTME/professional";
touch "$ABOUTME/professional/linkedin.txt";
echo "https://www.linkedin.com/in/$3" > "$ABOUTME/professional/linkedin.txt";

MYFRIENDS="$FOLDER/my_friends";
mkdir "$MYFRIENDS";
touch "$MYFRIENDS/list_of_my_friends.txt";
curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt> "$MYFRIENDS/list_of_my_friends.txt";

MYSYSTEM="$FOLDER/my_system_info";
mkdir "$MYSYSTEM";
touch "$MYSYSTEM/about_this_laptop.txt";
echo "My username: $1\nWith host: $(uname -a)" > "$MYSYSTEM/about_this_laptop.txt";
touch "$MYSYSTEM/internet_connection.txt";
ping -c 3 google.com > "$MYSYSTEM/internet_connection.txt";